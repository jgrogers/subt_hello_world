#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <actionlib/client/simple_action_client.h>

#include <darknet_ros_msgs/CheckForObjectsAction.h>
#include <tf2_ros/transform_listener.h>
#include <vision_msgs/Detection2DArray.h>
#include <algorithm>

// Receive and synchronize image and info
// Make action request to darknet_ros for classification
// Report detection poses. Translate relevant strings (suitcase, briefcase, etc)->backpack class for report
class DarknetObjectReporter {
  protected:
  ros::NodeHandle nh;
  ros::NodeHandle private_nh;
  typedef message_filters::sync_policies::ExactTime<sensor_msgs::Image, sensor_msgs::CameraInfo> MySyncPolicy;
  message_filters::Subscriber<sensor_msgs::Image> image_sub;
  message_filters::Subscriber<sensor_msgs::CameraInfo> info_sub;
  message_filters::Synchronizer<MySyncPolicy> sync;
  actionlib::SimpleActionClient<darknet_ros_msgs::CheckForObjectsAction> ac;
  double report_thresh;  // Confidence must be above this level to issue an artifact report
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener;
  std::string report_frame;
  ros::Publisher object_report_pub;
  public:
  DarknetObjectReporter(): 
    private_nh("~"),
    image_sub(nh, "image", 1),
    info_sub(nh, "camera_info", 1),
    sync(MySyncPolicy(10), image_sub, info_sub),
    ac("darknet_ros/check_for_objects", true),
    tfListener(tfBuffer) {
      private_nh.param("report_thresh", report_thresh, 0.75);
      private_nh.param("report_frame", report_frame, std::string("map"));
      object_report_pub = nh.advertise<vision_msgs::Detection2DArray>("objects", 1, false);
      ROS_INFO("Darknet object reporter:> waiting for connection to check for objects action server");
      ac.waitForServer();
      ROS_INFO("Darknet object reporter:> Connected to check for objects action server");
      // ExactTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
      sync.registerCallback(boost::bind(&DarknetObjectReporter::ImageInfoCb, this, _1, _2)); 

    }
  void ImageInfoCb(const sensor_msgs::ImageConstPtr& image, const sensor_msgs::CameraInfoConstPtr&info) {
    static int id = 0;
    // Make request via action interface to darknet_ros
    darknet_ros_msgs::CheckForObjectsGoal goal;
    goal.id = id++; 
    goal.image = *image;
    ac.sendGoal(goal);
    bool finished_before_timeout = ac.waitForResult(ros::Duration(2.0));
    if (finished_before_timeout) {
      actionlib::SimpleClientGoalState state = ac.getState();
      if (state == actionlib::SimpleClientGoalState::SUCCEEDED) {
        const darknet_ros_msgs::CheckForObjectsResultConstPtr result = ac.getResult();
        if (result->bounding_boxes.bounding_boxes.size() == 0) {
          ROS_INFO("Darknet object reporter:> No objects detected"); 
        } else {
          vision_msgs::Detection2DArray object_report;
          object_report.header = image->header;
          vision_msgs::Detection2D new_detection;
          new_detection.header = image->header; 
          for (size_t i = 0; i < result->bounding_boxes.bounding_boxes.size(); i++){
            ROS_INFO_STREAM("Darknet object reporter:> See a " 
                << result->bounding_boxes.bounding_boxes[i].Class
                << " with probability " << result->bounding_boxes.bounding_boxes[i].probability); 
            if (result->bounding_boxes.bounding_boxes[i].probability > report_thresh) {
              //Issue an artifact report. If the model is trained to use the correct strings, report directly
              //otherwise translate "suitcase" to "backpack" etc
              // This will be done by sending a detection2d array to the object_detection_node 
              vision_msgs::ObjectHypothesisWithPose ohp;
              std::string input_class =result->bounding_boxes.bounding_boxes[i].Class;  
              std::transform(input_class.begin(), input_class.end(), input_class.begin(), ::tolower);
              if (input_class == "backpack")
                ohp.id = 0;
              else if(input_class == "duct")
                ohp.id = 1;
              else if(input_class == "electricalBox")
                ohp.id = 2;
              else if(input_class == "extinguisher")
                ohp.id = 3;
              else if(input_class == "phone")
                ohp.id = 4;
              else if(input_class == "radio")
                ohp.id = 5;
              else if(input_class == "toolbox")
                ohp.id = 6;
              else if(input_class == "valve")
                ohp.id = 7;
              else if (input_class == "bottle")
                ohp.id = 3;
              else if (input_class == "suitcase")
                ohp.id = 0;
              ohp.score = result->bounding_boxes.bounding_boxes[i].probability;
              // For now, report the robot's pose as the detection pose
              geometry_msgs::TransformStamped transformStamped;
              try{
                transformStamped = tfBuffer.lookupTransform(report_frame, image->header.frame_id,
                    image->header.stamp, ros::Duration(0.5));
              }
              catch (tf2::TransformException &ex) {
                ROS_WARN("%s",ex.what());
                ros::Duration(1.0).sleep();
                continue;
              } 
              ohp.pose.pose.position.x = transformStamped.transform.translation.x;
              ohp.pose.pose.position.y = transformStamped.transform.translation.y;
              ohp.pose.pose.position.z = transformStamped.transform.translation.z;
              ohp.pose.pose.orientation.w = 1.0;
              new_detection.results.push_back(ohp);
            }
          }
          object_report.detections.push_back(new_detection);
          object_report_pub.publish(object_report);
        }
      } else {
        ROS_ERROR_STREAM("Darknet object reporter:> Received erroneous goal state " << state.toString());
      }

    } else {
      ROS_ERROR("Darknet object reporter:> Did not receive object report before timeout");
    }
  } 

};
int main(int argc, char** argv) {
  ros::init(argc, argv, "darknet_object_reporter");
  DarknetObjectReporter objreporter;

  ros::spin();
  return 0;
}
