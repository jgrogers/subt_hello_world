#include <ros/ros.h>
#include <vision_msgs/Detection2DArray.h>


#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/gazebo_client.hh>

#include <iostream>
//#include <commandpost_relay/ArtifactReport.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>


class LogicalCameraObjectReporter {
  protected:
    std::map<std::string, int> objects_to_recognize;
    ros::NodeHandle nh;
    ros::NodeHandle private_nh;
    std::string detection_frame_id;
    std::string report_frame_id;
    ros::Publisher detection_pub;
    ros::ServiceClient artifact_report_srv;
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener;
  public:
    LogicalCameraObjectReporter ():nh(), private_nh("~"),tfListener(tfBuffer) {
      //Populate objects_to_recognize from param server
      //
      private_nh.getParam("objects_to_recognize", objects_to_recognize);
      private_nh.param("detection_frame_id", detection_frame_id, std::string("base_link"));
      detection_pub = nh.advertise<vision_msgs::Detection2DArray>("object_detections", 1, false);
      //      artifact_report_srv = nh.serviceClient<commandpost_relay::ArtifactReport>("artifact_report");
    }
    void cb(const ConstLogicalCameraImagePtr &msg) {
      vision_msgs::Detection2DArray detection_msg;
      detection_msg.header.frame_id = detection_frame_id;
      detection_msg.header.stamp = ros::Time::now();

      for (size_t i = 0; i < msg->model_size(); i++) {
        if (msg->model(i).has_name() == false || msg->model(i).has_pose() == false) continue;
        std::string str = msg->model(i).name();
        str.erase(std::remove_if(str.begin(), str.end(), 
                                 []( auto const& c ) -> bool { return !std::isalpha(c); } ), str.end());
        std::transform(str.begin(), str.end(), str.begin(), 
                       [](unsigned char c){ return std::tolower(c); } // correct
                      );
         
        std::map<std::string, int>::const_iterator itr = objects_to_recognize.find(str);

        if (itr != objects_to_recognize.end()) {
          vision_msgs::Detection2D detection;
          detection.header.stamp = ros::Time::now();
          detection.header.frame_id = detection_frame_id;
          vision_msgs::ObjectHypothesisWithPose ohwp;
          ohwp.id = itr->second;
          ohwp.score = 1.0;
          ohwp.pose.pose.position.x = msg->model(i).pose().position().x();
          ohwp.pose.pose.position.y = msg->model(i).pose().position().y();
          ohwp.pose.pose.position.z = msg->model(i).pose().position().z();
          ohwp.pose.pose.orientation.x = msg->model(i).pose().orientation().x();
          ohwp.pose.pose.orientation.y = msg->model(i).pose().orientation().y();
          ohwp.pose.pose.orientation.z = msg->model(i).pose().orientation().z();
          ohwp.pose.pose.orientation.w = msg->model(i).pose().orientation().w();
          detection.results.push_back(ohwp);
          detection_msg.detections.push_back(detection); 
        }
      }
      if (detection_msg.detections.size() != 0) {
        detection_pub.publish(detection_msg);
      }
    }

};

/////////////////////////////////////////////////
int main(int _argc, char **_argv)
{
  // Load gazebo
  gazebo::client::setup(_argc, _argv);
  ros::init(_argc, _argv, "logical_camera_object_reporter");
  // Create our node for communication
  gazebo::transport::NodePtr node(new gazebo::transport::Node());
  node->Init();
  LogicalCameraObjectReporter lc_reporter;
  // Listen to Gazebo world_stats topic
  gazebo::transport::SubscriberPtr sub = node->Subscribe("~/X1alpha/X1alpha/base_link/logical_camera1/models",
    &LogicalCameraObjectReporter::cb, &lc_reporter);

  ros::spin();
  // Make sure to shut everything down.
  gazebo::client::shutdown();
  return 0;
}

